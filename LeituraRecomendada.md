# 1a Aula
* Darrel Huff - How to Lie With Statistics https://www.amazon.com/How-Lie-Statistics-Darrell-Huff/dp/0393310728

# 2a Aula
* Roman Pichler - Agile Product Management with Scrum https://www.amazon.com/Agile-Product-Management-Scrum-Addison-Wesley/dp/0321605780
* Niklas Modig - This is Lean - Resolving The Efficiency Paradox https://www.amazon.com/This-Lean-Resolving-Efficiency-Paradox/dp/919803930X
* James P. Womack - The Machine that Changed The World https://www.amazon.com/Machine-That-Changed-World-Revolutionizing/dp/0743299795
* Prince 2 Agile - Axelos https://www.amazon.com/Prince2-Agile-Stationery-Office/dp/0113314671
* Eric Ries - The Lean Startup https://www.amazon.com/Summary-Lean-Startup-Entrepreneurs-Continuous/dp/1721514066
* Steve Blank - The Four Steps to the Epiphany https://www.amazon.com/Four-Steps-Epiphany-Steve-Blank/dp/0989200507
* Paulo Caroli - Direto ao ponto https://www.amazon.com/Direto-Ponto-Criando-produtos-Portuguese-ebook/dp/B019NG697E
* Ken Schwaber - Agile Project Development with Scrum https://www.amazon.com/Agile-Project-Management-Developer-Practices/dp/073561993X

# 3a Aula
* Craig Larman - Large Scale Scrum https://www.amazon.com/Large-Scale-Scrum-More-Addison-Wesley-Signature/dp/0321985710
* Jurgen Appelo - Management 3.0 https://www.amazon.com/Management-3-0-Developers-Developing-Addison-Wesley/dp/0321712471
* Jurgen Appelo - How to Change the World - Management 3.0 https://www.amazon.com/How-Change-World-Management-3-0-ebook/dp/B007ZT2KES/
* Autor: Eunice Alencar https://www.amazon.com/s/ref=dp_byline_sr_ebooks_1?ie=UTF8&text=Eunice+Soriano+de+Alencar&search-alias=digital-text&field-author=Eunice+Soriano+de+Alencar&sort=relevancerank
* Jeffrey Hiatt - ADKAR: A Model for Change in Business, Government and our Community https://www.amazon.com/ADKAR-Change-Business-Government-Community/dp/1930885504

# 5a aula
* Gene Kim - The DevOps Handbook: How to Create World-Class Agility, Reliability, and Security in Technology Organizations  https://www.amazon.com/Discover-Deliver-Product-Planning-Analysis/dp/0985787902
* Jez Humble - Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation https://www.amazon.com/Continuous-Delivery-Deployment-Automation-Addison-Wesley/dp/0321601912
  * https://continuousdelivery.com/
* Jeff Patton - User Story Mapping https://www.amazon.com/User-Story-Mapping-Discover-Product/dp/1491904909
  * https://jpattonassociates.com/
* Ellen Gottesdiener - Discover to Deliver - Agile Product Planning and Analysis https://www.amazon.com/Discover-Deliver-Product-Planning-Analysis/dp/0985787902
