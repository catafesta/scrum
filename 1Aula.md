Primeira Aula: 13/08/2018

20:00

https://www.youtube.com/watch?v=WS0Wkz5fmX8

Notas:

# Slides
## Preparatório para a certificação PSPO I & PSM 1

## O que é o curso?
* Formato do curso: 20:00 às 22:30.
* Totalmente gratuito ts 20:18.
* Trata-se da sétima ou oitava turma.
* Vídeos vão ficar disponíveis por aproximadamente duas semanas, até 48 horas após aula.
* Não há controle de presença ts 20:13.
* Não se trata de certificado de Master Product Owner ts 20:18.
* Público alvo restrito a empregados/servidores de empresas públicas ts 20:20.
* Curso é refinado a cada turma ts 20:21. // Nice, bate com o espírito do Scrum.
* O material pode (e deve) ser compartilhado entre colegas. ts 20:28.
* O material (links, acessos, slides, etc) não pode ser vendido ou utilizado para fins comerciais ou divulgado na rede (internet).
* Índice de aprovação de 90% nas provas do PSPO e PSM ts 20:51.

## Frases de Efeito
* Os empregados de empresa pública no Brasil são os que tem maior conhecimento entre a categoria, no mundo. ts 20:23.
* Nós seremos os empregados mais feras que o mundo já conheceu. ts 20:26.
* A inovação não acontece nos ambientes simples ou complicados, e sim nos ambients complexos, onde há feedback. É ele que possibilita a emergência de uma solução. Cynefin Framework

## Softwares
* Há um software chamado socrative. Ele vai ser utilizado para fazer simulados, e tem versão para smartphone e pc. - https://www.socrative.com/ !todo pesquisar se ele tem versão web.
// Os simulados somente vão ser entregues no final do curso ts 2a aula ts 22:34.
* Google Classroom. - https://classroom.google.com/h

## Conceitos
* Informação precisa ser compartilhada, e não colecionada. (Collecting the Dots vs Connecting the Dots) ts 20:36.

## Tarefas
* Leia o Scrum Guide.
<!-- link postado no grupo do telegram às 21:37 por Sephora -->
https://www.scrumguides.org/docs/scrumguide/v2017/2017-Scrum-Guide-Portuguese-Brazilian.pdf
<!-- Obtido no mesmo site -->
https://www.scrumguides.org/docs/scrumguide/v2017/2017-Scrum-Guide-US.pdf
* Assista ao vídeo "Scrum em 9 minutos".
<!-- link postado no grupo do telegram às 21:37 por Sephora -->
https://youtu.be/XfvQWnRgxG0

## ETO ts 20:40 a 20:50
* Grupo de Funcionários do Banco do Brasil que trabalha com inovação ts 20:38.
* Intraempreendedores.
* Incentiva a transformação de dentro pra fora.
* Design de soluções centrado no ser humano.
* Conceito de Lideranças Circustanciais - !todo //Modelo Horizontal da Valve e Hierarquia da Microsoft.
* Comunidade na Intranet !todo Checar URL da comunidade - checar assunto "Blockchain".
* Desafio: Provocar a mudança cultural.
* Lema: Se vocÊ não participa da conversa, a conversa acontece **sem você**.
* Comunidade no Conecte: https://connections.bb.com.br/communities/service/html/communitystart?communityUuid=ce8716a1-3575-44fd-8b2e-4f5360fe03e1

## Bookcrossing ts 20:50
* Troca de Livros conduzido através de mensagem pessoal do whatsapp com Sephora Lilian.
* Fórum no Conecte: https://connections.bb.com.br/forums/html/forum?id=64e4b805-e146-4ed6-911e-8e3af44cb107

## Sucesso de um produto
// !todo referência do livro do Sutherland.
* O que diferencia um bom produto de um mau produto?
  * 80% da funcionalidade de um produto não é usada com frequência pelo usuário final. ts 21:00
  * 20% da funcionalidade de um produto é o que entrega o verdadeiro valor. ts 21:02
* A aceleração tecnológica acaba tornando projetos obsoletos. "O mundo que você encontra hoje não é mais o mundo que você encontra depois". ts 21:21.
* Referência: Back to the Future. 21:25

## Modelo em Cascata ts 21:29
* 1960
* Preditivo, dividido em fases / etapas.
* Solicitações de mudanças no projeto são tratadas com hostilidade.
* Problemas:
  * As pessoas não tem a visão completa no projeto. Os detalhes são muito complexos.
  * Os clientes tem dificuldade para expressar tudo o que desejam.
  * A medida em que o projeto é construído, os desejos do cliente mudam. A realidade muda.
  * Curva de aprendizado expressiva a cada troca de integrante.
  * Atender a mudanças no projeto usa recursos de emergência, aumentando o débito técnico do produto. // "Dívida Técnica"
  * Cultura de Culpa.
  ---
  * SUTHERLAND: Capítulo 1 - Cultura da Mentira

## Manifesto Ágil ts 22:11
* 2001
* Foca na mudança dos desejos do cliente.
* Focos:
  * Indivíduos e Interações, e não Processos e Ferramentas.
  * Produto Funcionando, e não Documentação Extensa.
  * Confiança, e não Burocracia.
  * Colaborar com o Cliente, e não Negociar Contratos.
  * Responder a Mudanças, e não Seguir Planos.
  * Resultado, e não Escopo.

## Leitura Recomendada
* Darrel Huff - How to Lie With Statistics. ts 20:58 https://www.amazon.com/How-Lie-Statistics-Darrell-Huff/dp/0393310728
* SUTHERLAND
https://www.amazon.com/Scrum-Doing-Twice-Work-Half/dp/1847941109/
