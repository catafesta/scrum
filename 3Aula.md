Terceira Aula: 15/08/2018

20:00

Prévia Slides: https://www.dropbox.com/s/trx0bb40ouxws39/CAIXA%20PSPO_Full_v3.pptx?dl=0
(sim, é um arquivo de 564 mega)

@Gino Terentim Junior
> Vou disponibilizar a vocês, também, o *arquivo "completo"*, caso queiram navegar por ele antes das aulas. Os slides, muitas vezes, *não são fáceis de entender*, então, recomendo utilizarem sempre com cautela, pois posso modificar um ou outro slide ao longo do curso e, além disso, eles não dispensam a explicação e a interação com a turma pra refletirmos e sedimentarmos os conceitos.

https://www.youtube.com/watch?v=S-zB7OCEmDc

511 watching now ts 21:11:02
580 watching now ts 21:39:58
550 watching now ts 22:26:21
---

## Reverse Experience - daniel ts 20:00:39

* Exercitar o timebox é um desafio 20:15:06
* Escolher o que não fazer, para as organizações, é imensamente dolorido.

## Papéis
* Scrum Master ts 20:18:51
  * Gestor dos processos, e não das pessoas.
  * Funções:
    * Comemora as vitórias do time. Isso faz parte do desenvolvimento da equipe ts 20:26:03
    * Mediação de Conflitos. O foco é ensinar as pessoas a resolverem seus próprios problemas
    * Remover impedimentos. ts 20:26:35 // Chewbacca em The Empire Strikes Back
      * Fazer pela equipe prejudica o desenvolvimento da equipe. ts 20:47:41
      * Controlar a complexidade e garantir a previsibilidade do ambiente. ts 20:31:56
    * Ser a referência do Scrum para o time. O Ri do Shu Ha Ri.
  * Perfil !slides
    * Estudioso, desenvolve o time e toda a organização.
  * Não faz parte do time de desenvolvimento ts 20:28:27
  * Não é o gerente do projeto.
  * Pode ser trocado, mas por decisão do time. ts 20:34:10
  * Pode ser o PO, numa equipe Lean. Não é aconselhado. ts 20:52:34
* Dev Team ts 20:56:18
  * Tamanho - ts 20:59:25
    * De 3 a 9 pessoas. 6 é o tamanho ideal.
    * Times com muitas pessoas tem muitos canais de comunicação, o que explode a complexidade dos processos.
  * Função
    * Construir o produto.
    * Determinam o que deve ser feito para alcançar a meta do Sprint.
  * Perfil !slides
    * Auto Organizado
    * Conhecimento do Scrum.
    * Multidisciplinar. A equipe também precisa respeitar as diferenças. ts 21:53
    * Não
      * Individual

  * Trocas de pessoal impactam a produtividade. É recomendando que as trocas sejam feitas entre uma sprint e outra.

## Frases de Efeito
* O ambiente criativo não resolve problemas de cultura. ts 20:39:31.
  * Arquimedes
    * como o ourives acumula riquezas?
    * como medir a quantidade de ouro e prata de uma coroa?
      * comparando volume deslocado de água entre os dois corpos com o mesmo peso.

## Conceitos
* "Shu Ha Ri" - Artes Marciais
  * Shu - Siga a regra. Aprenda.
  * Ha - Quebre a regra. Inove.
  * Ri - Seja a regra. Torne-se referência.
* Linguagem utilizada na prova e no Scrum Guide ts 20:52:55
  * Must Not - É proibido.
  * Should/May - Faz parte das boas práticas / É recomendado.
  * Must - É obrigatório.
* Scrum escalado - Scrum com múltiplas equipes de desenvolvimento

## Gestão ao longo do tempo ts 21:33:16
* Mecanização - Garanta a execução da tarefa. Management 1.0.
* Eletricidade - Garanta a qualidade da produto. Management 2.0.
* Computação - !todo
* Colaboração -

  !todo slides quotes pessoas importantes na gestão.
  * Mary Parker Follet
    * Os princípios da administração científica (TAYLOR) não são suficientes pra tratar de problemas complexos. Esses problemas exigem criatividade.
  * Edwards Deming
    * "As pessoas já estão fazendo o seu melhor, os problemas estão com o sistema. Somente a gestão pode mudar o sistema."
  * Jurgen Appelo
    * "Gerencie o sistema, não as pessoas."
  * Peter Diamandis - abundância - o futuro é melhor do que você imagina
* Problem Solving in Business and Management

## ETO Como iniciar um movimento? ts 21:16:29
* Derek Sivers - How to Start a Movement https://www.youtube.com/watch?v=RXMnDG3QzxE
  * Fazer > Dizer
  * Legendado - https://www.youtube.com/watch?v=XOhiBna3h-s
* Para criar a conexão, é necessário um ambiente em comum.
* O ETO iniciou num curso sobre "futurismo na perestroika".
* Construiu-se um manifesto e uma comunidade.
* Palestras no formato VLW FLW.
* Movimentos no formato "On Fire".
* Valores: Comunicação direta.

## A importância da gestão da mudança
!slides ts 22:06:11
* A mudança ocorre em fases. Exemplo: parar de fumar. ts 22:10:16
  * Consciência  - Confusão - Entendo que preciso parar de fumar.
  * Desejo - Resistência - Parar de fumar me irrita.
  * Conhecimento - Medo/Ansiedade - Tenho medo de ficar sem os efeitos do cigarro.
  * Habilidade - Frustração - Não consigo parar de fumar.
  * Reforço - Regressão - Paro de fumar todos os dias.

## A ciência multitarefa - gerandoaguias
* Achamos que nosso cérebro aceita mais que uma tarefa. Porém não é bem assim. Quando o cérebro troca de uma tarefa pela outra, existe um intervalo de tempo que é trocado entre as duas tarefas.
  * https://medium.com/@ccanipe/stop-trying-to-multitask-you-re-terrible-at-it-a61747e112e5
  * https://www.huffingtonpost.com/rhiannon-giles/hells-jenga_b_9391514.html
* O multitasking dificulta o aprendizado. ts 22:23:11

## Leitura Recomendada
* Craig Larman - Large Scale Scrum https://www.amazon.com/Large-Scale-Scrum-More-Addison-Wesley-Signature/dp/0321985710
* Jurgen Appelo - Management 3.0 https://www.amazon.com/Management-3-0-Developers-Developing-Addison-Wesley/dp/0321712471
  * Equipes ágeis precisam de limites. Esses limites não podem ser cercas elétricas. Eles precisam ser playgrounds, pra equipe poder cair, mas também poder aprender. ts 21:47:30
* Jurgen Appelo - How to Change the World - Management 3.0 https://www.amazon.com/How-Change-World-Management-3-0-ebook/dp/B007ZT2KES/
* Autor: Eunice Alencar https://www.amazon.com/s/ref=dp_byline_sr_ebooks_1?ie=UTF8&text=Eunice+Soriano+de+Alencar&search-alias=digital-text&field-author=Eunice+Soriano+de+Alencar&sort=relevancerank
    // Autora brasileira sobre criatividade (foi mencionado no chat do zoom, não na palestra em si)
* Jeffrey Hiatt - ADKAR: A Model for Change in Business, Government and our Community https://www.amazon.com/ADKAR-Change-Business-Government-Community/dp/1930885504
