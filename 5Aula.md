Quarta Aula: 22/08/2018

20:00

https://www.youtube.com/watch?v=c86vLmGPhKc

---

!todo revisar aula

8/22/2018, 9:44:10 PM

* Convergência https://www.youtube.com/watch?v=ibdzYyMWpzw

## TCO

* Há um gap enorme entre o preço da entrega e o custo total do produto.
8/22/2018, 9:47:02 PM
* O custo de reparo e da manutenção não costumam ser levados em conta no desenho do produto.
* Débito técnico é uma das grandes causas de um TCO desfavorável.
* Grupo Gartner - estratégia para maquiar o TCO, escondendo o custo de uma camada em outra.
Ao invés de priorizar o valor, é priorizada a percepção de baixo custo.

### Como melhorar o ROI sobre o TCO?

* Priorizar a percepção de valor.
* Entregar rápido.
* Lidar com feedback.
* Corrigir erros e refatorar código (controlar o débito técnico).
* Automatizar testes.

O objetivo de entregar rápido é aprender rápido. Utilizar a metodologia ágil para entregar rápido um projeto que o cliente não quer, ou algo com um escopo maior do que o necessário, é desperdício.


 Purchase, Repairs, Maintenance, Upgrades, Service, Support, Security, Training.


O ágil não é pra entregar o projeto mais rápido.
Ao acelerar a entrega de produtos

## Artefatos

* Backlog
  * Contém idéias de incremento do produto.
  * User Story Mapping - método de construção e de administração do backlog.
    * Backbone: o que é primordial para sustentar o produto. !todo revisar para ver se ele é conjunto de temas.
    * Walking Skeleton: O que é necessário para entregar o valor ao produto. MUST
    * O resto dos itens é elencado em ordem de prioridade. SHOULD e COULD
    ---
    * A melhor forma de utilizar não é somente no início do projeto, como no modelo cascata. Ela deve ser utilizada durante todo o produto, para designar em qual estado ele está.

* Roadmap - Plano de Entrega das Releases.

## Ritos/Eventos
* Sprint Planning
  * Reunião, dividida em duas metades.
  * Timebox: 4 horas para sprint de 30 dias. Proporcional.
  ---
  * Primeira Metade:
    * Revisão do backlog do produto.
    * Mensuração da Velocidade do Time.
    * Definição de pronto.
    * Compromissos da Retrospectiva.
  * Segunda Metade:
    * Meta do Sprint
      * O que consigo fazer durante a sprint para alcançar a meta?
      * PO pode cancelar o sprint se essa meta for obsoleta.
    * Estimativas
    * Backlog do Sprint

## Leitura Recomendada

* Gene Kim - The DevOps Handbook: How to Create World-Class Agility, Reliability, and Security in Technology Organizations  https://www.amazon.com/Discover-Deliver-Product-Planning-Analysis/dp/0985787902
* Jez Humble - Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation https://www.amazon.com/Continuous-Delivery-Deployment-Automation-Addison-Wesley/dp/0321601912
  * https://continuousdelivery.com/
* Jeff Patton - User Story Mapping https://www.amazon.com/User-Story-Mapping-Discover-Product/dp/1491904909
  * https://jpattonassociates.com/
* Ellen Gottesdiener - Discover to Deliver - Agile Product Planning and Analysis https://www.amazon.com/Discover-Deliver-Product-Planning-Analysis/dp/0985787902
