Segunda Aula: 14/08/2018

20:00

https://www.youtube.com/watch?v=4V20zRH9zXo

820 watching ts 21:18

Notas:

* ts 20:10 é mais importante entregar algo de valor do que entregar algo perfeito.

## Formato do curso
* ts 20:17 A cada aula é escolhido um participante para participar da "reverse experience", dando um resumo da aula anterior.
* ts 22:31 A organização é feita através de um grupo. "Fique de olho no grupo, a mensagem vai aparecer."
* Os slides são licenciados pelo creative commons. ts 20:31

## Equipes Ágeis - Continuação
* Equipes ágeis são abertas à mudança. Elas buscam a mudança, ao invés de se proteger dela. ts 20:19
* Entrega incremental e iterativa.
* O feedback é o óleo da engrenagem. Valor é uma medida pessoal. Ele somente pode ser medido após a entrega do produto. ts 20:23

## Scrum
* !todo date
* Originada de um termo de jogo de rugby ts 21:49 !todo SUTHERLAND
  * Não é uma pessoa que sustenta a jogada e aguenta a pressão, e sim o time todo.
  * A equipe deve funcionar como um organismo.
* Iterativo e Incremental. O produto não fica pronto na primeira entrega. ts 20:23
  * O feedback das versões anteriores é utilizado para aprendizado.
  * O principal objetivo é aprender. ts 20.24
  * Métrica de Ouro: "O quanto sua equipe aprendeu?"
* Leve e simples de entender, porém difícil de dominar.
  * Porque é difícil dominar e aplicar?
    * Por causa da cultura ts 20:34. Acumular poder, acumular informação. ts 21:27
    * Substitui o "vai lá e faz" por "vai lá, planeje, e faça, e; sobretudo; aprenda". O mínimo de planejamento possível. ts 20:39
    * Apego a procedimentos desnecessários. Busca de escopo ao invés de busca por valor. ts 20:48
    * As estruturas tradicionais não reconhecem a capacidade das pessoas de se autogerenciar. ts 21:48
    * Os POs nem sempre tem autonomia ts 22:17.
    * O scrum é tratado como uma bala de prata e é morto na primeira oportunidade pela hierarquia. ts 22:32
* Framework para desenvolver e manter produtos complexos.
  * Apesar de ser criado para software, seus maiores casos de sucesso de sucesso estão em outras áreas. ts 20:30
  * SUTHERLAND !todo jardim de infância.    
  ---
  Composto por !todo - copiar tabela, slide estrutura do framework
  * Cerimônias/Eventos
    * Sprint
      * Contém todos os outros eventos do Scrum.
      * Período máximo de 30 dias.
      * Principal Evento do Scrum ts 20:49
      * Normalmente, as Sprints duram o mesmo tempo - além de *timeboxed*, ele é estável/imutável.
      * Objetivo: reduzir a complexidade / aumentar a previsibilidade. ts 20:51
        // Objetivo extra - SUTHERLAND, DUHIGG: criar hábitos
      * Como determinar a duração? Considere
        * Tempo entre uma release e outra.
        * Nível de incerteza.
        * Disponibilidade de feedback.
        * Equilíbrio do trabalho em cada sprint.
        * Velocidade e aceleração do time. // Pergunta dura: O que foi feito na última sprint, pode ser repetido? ts 21:05
      * A prática do Sprint0, o sprint criado para testar a equipe, e, entre outras coisas, determinar a duração dos outros sprints, não faz parte do Scrum ts 21:07.
      * Estrutura:
        * Planejamento
        * Execução
          * Reunião diária
            * Objetivo: previsibilidade. ts 21:11
            * Somente a equipe fala. Ela é aberta. ts 21:20
        * Apresentação
        * Retrospectiva

    * Release / Entrega
      * Intervalo e formato definido pelo po ts 20:58

  * Papéis
    * Time / Equipe Scrum
      * Composto por PO, e Dev Team
      * PO
        * É uma pessoa. Não um comitê. ts 22:09
        * O grande PO é humilde. ts 21:00 //Indiana Jones and The Last Crusade - desafios de humildade, pulo da fé.
        * Nâo representa o cliente ts 21:53
        * Função: Maximizar o valor do trabalho realizado pela equipe. ts 21:55
        * Foco: valor, prioridades.
        * Decide sobre as releases, seus intervalos e sua entrega.
        * Representa os clientes, usuários e stakeholders, e age como interface entre ele e o resto do time.
          * O contato com o resto do mundo precisa ser regulado, para garantir a previsibilidade e prevenir o caos. ts 22:18. Isso também é função do scrum master. Aula3 ts 15/08/2018, 20:31:56
        * Precisa conhecer sobre o negócio e sobre as personas.
        * Precisa ter autonomia pra tomar decisões sobre o produto. ts 22:16
        * Deve estar disponível para conversar com o time de desenvolvimento, quanto necessário ts 22:22.
      * Dev Team / Time de Desenvolvimento
        * Foco: entrega, valor, uso.
        * Função: !todo
        * Multidisciplinar.
      * Responsável por fazer o trabalho.
      * Não há lider. Não há gerente de projetos. Não há gestor. ts 21:52
      * Scrum Master
        * Função: resolver problemas, derrubar barreiras. ts 22/01 // The Empire Strikes Back - função do chewbacca? !todo checar nome
        * Foco: processos, desenvolvimento.
    * Usuários (não pagam) / Clientes ts 21:54
      * É necessário saber o que representa o valor pro cliente, mesmo quando o cliente não sabe o que quer. ts 21:59
    * Stakeholder
      * Galinhas - ts 22:10

  * Artefatos
    * Backlog
      * Cada produto tem somente um backlog. ts 22:09
      * Quem altera a prioridade dos itens é o PO.
      * Itens dentro de um Sprint não podem ser alterados. ts 21:14
      * Os itens precisam ser pequenos o suficiente para caberem na Sprint.
    * Produto
      * Nunca está definitivamente pronto, pois o Scrum tem visão incremental/iterativa. ts 22:12
      * Produto pronto é produto morto.

* Não é prescritivo, preditivo, !todo copiar slides
* É construído a partir do Lean e do Agile
* Pilares !todo checar slides
  * teoria dos processos empíricos ts 20:39
    * transparência
      * no scrum não se sonega informação ts 21:21
    * inspeção
      * os processos precisam ser observados. o que posso fazer melhor? o que não fiz bem? **o que posso deixar de fazer?**. ts 21:22
    * adaptação !todo checar slides ts 21:47
      * como melhorar, como garantir que o deu errado não ocorra novamente, como garantir que o que deu certo vai acontecer?

## Conceitos
* Timebox:
  * É um conceito de unidade de tempo que é imutável. Ele é utilizado nas Springs.
  * A quantidade de tempo não poderá aumentar caso algum problema ou novo requisito seja identificado.
  * Exemplos: dia, semana. ts 20:42
* Delegar:
  * Delegar não é delargar. A responsabilidade precisa ser assumida por quem delega. Delega-se uma tarefa, não responsabilidade. ts 21:17
* Personas
  * Ao mapear o caso de usos dos usuários extremos, é possível desenhar um produto mais completo ts 22:30.
    * Ao trabalhar somente com a média/mediana, não é possível sair da mediocridade.

## Leitura Recomendada
* Roman Pichler - Agile Product Management with Scrum https://www.amazon.com/Agile-Product-Management-Scrum-Addison-Wesley/dp/0321605780
* Niklas Modig - This is Lean - Resolving The Efficiency Paradox https://www.amazon.com/This-Lean-Resolving-Efficiency-Paradox/dp/919803930X
* James P. Womack - The Machine that Changed The World https://www.amazon.com/Machine-That-Changed-World-Revolutionizing/dp/0743299795 // método toyota
* Prince 2 Agile - Axelos https://www.amazon.com/Prince2-Agile-Stationery-Office/dp/0113314671
  // Versão Kindle custa USD 74,64
* Eric Ries - The Lean Startup https://www.amazon.com/Summary-Lean-Startup-Entrepreneurs-Continuous/dp/1721514066
* Steve Blank - The Four Steps to the Epiphany https://www.amazon.com/Four-Steps-Epiphany-Steve-Blank/dp/0989200507
* Paulo Caroli - Direto ao ponto https://www.amazon.com/Direto-Ponto-Criando-produtos-Portuguese-ebook/dp/B019NG697E
* Ken Schwaber - Agile Project Development with Scrum https://www.amazon.com/Agile-Project-Management-Developer-Practices/dp/073561993X
  * Conceito: porcos e galinhas. !slides
    * Porcos são comprometidos com o projeto.
    * Galinhas somente são envolvidas com o projeto.
    * Moral da história - o time de dev é composto pelos porcos. É por isso que as galinhas não participam da reunião ts 22:08.

## Origem do curso - Thiago ts 21:36
* Como melhorar a situação da nossa empresa? Como melhorar a situação do nosso país?
* A única maneira de mudar é através da educação, do aprendizado.
* Fronteira de Conhecimento / Noção do Desconhecido
  * Somente quando há contato com o desconhecido, temos noção dele.
  * Quanto maior o conhecimento, maior essa fronteira. Maior a noção do desconhecimento.
  * O contato entre as pessoas normalmente é feito através das zonas de conflito, o decote entre as Fronteiras de Conhecimento.
  * Ao encontrar um ponto em comum, há convergência de opiniões. // !slide "onde a magia começa a acontecer"
* O mundo está caminhando para uma realidade diferente. É necessário criar um ambiente fértil para a colaboração.
* Paradoxo de Sócrates // checar referência ts 21:46
