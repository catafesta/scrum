<!-- postado por Sephora Liiiam no telegram em 20/08/2018 11:22:22  -->
<!-- todo usar como referência -->

Relação de :book:

*Aula 1*
-Como Mentir com Estatística
Autor: Darrell Huff

*Aula 2*
-This is lean (Isto é Lean): Resolving the Efficiency Paradox
Autor: Niklas Modig

-The Four Steps to the Epiphany: Successful Strategies for Products That Win
Autor: Steve Blank

- The Lean Startup: How Constant Innovation Creates Radically Successful Businesses
Autor: Eric Ries

- PRINCE2 Agile
The Stationery Office

- The Machine That Changed the World: The Story of Lean Production-- Toyota's Secret Weapon in the Global Car Wars That Is Now Revolutionizing World Industry
Autor: James P. Womack e Daniel T. Jones

- Agile Project Management with Scrum
Autor: Ken Schwaber

-Direto ao Ponto Criando produtos de forma enxuta
Autor: Paulo Caroli

- Agile Product Management with Scrum: Creating Products that Customers Love
Autor: ROMAN PICHLER

*Aula 3*
- Large-Scale Scrum: More with Less
Autor: Craig Larman e Bas Vodde

- Management 3.0: Leading Agile Developers, Developing Agile Leaders
Autor: Appelo Jurgen

- Abundância
Autor: Peter H. Diamandis e Steven Kotler

- ADKAR: A Model for Change in Business, Government and our Community
Autor: Jeffrey Hiatt

*Aula 4*
-Scrum - A Pocket Guide
Autor: Gunther Verheyen

-A Startup Enxuta
Autor: Eric Ries

-Diffusion of Innovations
Autor: Everett M. Rogers

-Project Model Canvas
Autor: Jose Finocchio Jr

-Scrum Essencial
Autor: Roberto Rezende

- O ego é seu inimigo: Como dominar seu pior adversário
Autor: Ryan Holiday

-Organizações Exponenciais
Autor: Ismail Salim

-Reinventando As Organizações
Autor: Frederic Laloux
